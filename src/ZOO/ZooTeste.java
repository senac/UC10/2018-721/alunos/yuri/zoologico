/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ZOO;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sala304b
 */
public class ZooTeste {
    
    public static void main(String[] args) {
        Gato gato = new Gato();
        Cachorro cachorro = new Cachorro();
        Lagarto lagartixa = new Lagarto();
        Ornitorrinco ornitorrinco = new Ornitorrinco();
        Pato pato = new Pato();
        Sapo sapo = new Sapo();
        Galinha galinha = new Galinha();
        Cobra cobra = new Cobra();
        
        List<Animal> ColecaoAnimal = new ArrayList<>();

        ColecaoAnimal.add(gato);
        ColecaoAnimal.add(cachorro);
        ColecaoAnimal.add(cobra);
        ColecaoAnimal.add(galinha);
        ColecaoAnimal.add(lagartixa);
        ColecaoAnimal.add(pato);
        ColecaoAnimal.add(sapo);
        ColecaoAnimal.add(ornitorrinco);

        for (int i = 0; i < ColecaoAnimal.size(); i++) {
            ColecaoAnimal.get(i).andar();
            ColecaoAnimal.get(i).falar();

        }
        
        
    }
    
}
